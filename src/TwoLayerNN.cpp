/*
 * TwoLayerNN.cpp
 *
 *  Created on: Apr 15, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/TwoLayerNN.hpp>

namespace re2uta
{

Eigen::VectorXd sigmoid(Eigen::VectorXd& z)
{

    for (uint i = 0; i < z.size(); i++)
    {
        z(i) = 1.0 / (1.0 + exp(-z(i)));
    }

    return z;

}



TwoLayerNN::TwoLayerNN( int inputDimArg,
                        int outputDimArg,
                        int hiddenLayerSizeArg,
                        double kappaArg,
                        double kpArg,
                        double kdArg,
                        double gFactor,  // innerLayerLearningRate
                        double fFactor ) // lastLayerLearningRate
{
    //Initialize the matrices
    inputDim         = inputDimArg;        // Size of the inputs
    outputDim        = outputDimArg;       // Size of the outputs
    hiddlenLayerSize = hiddenLayerSizeArg; // Size of the hidden layer


    //delT = 0.0001;
    kappa = kappaArg; //0.3
    kp    = kpArg; // prop. gain for PID inner loop
    kd    = kdArg; //*std::sqrt(Kp); // der. gain for PID inner loop
    kz    = 10;
    zb    = 5;


    G.resize(inputDim + 1, inputDim + 1);
    G = gFactor*Eigen::MatrixXd::Identity(inputDim + 1, inputDim + 1);


    F.resize(hiddlenLayerSize,hiddlenLayerSize);
    F = fFactor*Eigen::MatrixXd::Identity(hiddlenLayerSize,hiddlenLayerSize);

    L.resize(outputDim,outputDim);
    L = kp*Eigen::MatrixXd::Identity(outputDim,outputDim);


    //TODO determine which is randomly initialized
    V_trans.resize(hiddlenLayerSize,inputDim + 1);
    V_trans = Eigen::MatrixXd::Random(hiddlenLayerSize,inputDim + 1);

    W_trans.resize(outputDim, hiddlenLayerSize);
    W_trans = Eigen::MatrixXd::Random(outputDim, hiddlenLayerSize);

    V_trans_next.resize(hiddlenLayerSize,inputDim + 1);
    V_trans_next = Eigen::MatrixXd::Random(hiddlenLayerSize,inputDim + 1);

    W_trans_next.resize(outputDim, hiddlenLayerSize);
    W_trans_next = Eigen::MatrixXd::Zero(outputDim, hiddlenLayerSize);


    Z.resize(hiddlenLayerSize+inputDim+1,hiddlenLayerSize+outputDim);
    Z = Eigen::MatrixXd::Zero(hiddlenLayerSize+inputDim+1,hiddlenLayerSize+outputDim);
    Z.block(0,0,hiddlenLayerSize,outputDim) = W_trans.transpose();
    Z.block(hiddlenLayerSize,outputDim,inputDim+1,hiddlenLayerSize) = V_trans.transpose();



    r.resize(inputDim,1);
    r = Eigen::VectorXd::Zero(inputDim,1);

    vRobust.resize(inputDim,1);
    vRobust = Eigen::VectorXd::Zero(inputDim,1);




    x.resize(inputDim + 1,1);
    x = Eigen::VectorXd::Random(inputDim + 1,1);
    x(0) = 1;

    y.resize(outputDim,1);
    y = Eigen::VectorXd::Random(outputDim,1);

    hiddenLayer_in.resize(hiddlenLayerSize,1);
    hiddenLayer_in = Eigen::VectorXd::Zero(hiddlenLayerSize,1);

    hiddenLayer_out.resize(hiddlenLayerSize,1);
    hiddenLayer_out = Eigen::VectorXd::Zero(hiddlenLayerSize,1);

    outputLayer_out.resize(outputDim,1);
    outputLayer_out = Eigen::VectorXd::Zero(outputDim,1);
}

TwoLayerNN::~TwoLayerNN(){}

Eigen::VectorXd TwoLayerNN::getFilteredError(){ return r; }

Eigen::VectorXd TwoLayerNN::getRobustifyingSignal()
{
    Z.block(0,0,hiddlenLayerSize,outputDim) = W_trans.transpose();
    Z.block(hiddlenLayerSize,outputDim,inputDim+1,hiddlenLayerSize) = V_trans.transpose();

    double frobZ;
    frobZ = Z.norm();

    vRobust = -kz*(frobZ + zb)*r;
    return vRobust;
}


void TwoLayerNN::setKp( double value )
{
    kp = value;
}

void TwoLayerNN::setKd( double value )
{
    kd = value;
}

// This would give the W'sigma(V'x)
Eigen::VectorXd
TwoLayerNN::NNAug_out( const Eigen::VectorXd& qDes,
                       const Eigen::VectorXd& q,
                       const Eigen::VectorXd& qDesDot,
                       const Eigen::VectorXd& qDot,
                       double dt)
{
    // Filtered error
    r = (qDesDot - qDot) + kp*(qDes - q);
    x << 1,
         q,
         qDot;

    hiddenLayer_in  = V_trans*x;
    hiddenLayer_out = sigmoid(hiddenLayer_in);
    outputLayer_out = W_trans*hiddenLayer_out;
    y               = outputLayer_out;

    Eigen::MatrixXd identity = Eigen::MatrixXd::Identity(hiddenLayer_out.rows(),hiddenLayer_out.rows());

    Eigen::MatrixXd sigmaPrime;
    sigmaPrime = hiddenLayer_out.asDiagonal()
                    * (   (identity                             )
                        - (identity*hiddenLayer_out.asDiagonal())
                      );

    VAug_update( q, r, sigmaPrime, dt);
    V_trans = V_trans_next;

    WAug_update( q, r, sigmaPrime, dt);
    W_trans = W_trans_next; //FIXME verify this actually works

    return y;
}



//This would give the update V(k+1) = V(k) + V'sigma(V'x)
void TwoLayerNN::VAug_update( const Eigen::VectorXd& q, const Eigen::VectorXd& r, const Eigen::MatrixXd & sigmaPrime, double delT )
{
    Eigen::MatrixXd temp = (sigmaPrime.transpose()*W_trans.transpose()*r);
    V_trans_next.transpose() = V_trans.transpose() + (G*x*temp.transpose() - kappa*G*r.norm()*V_trans.transpose())*delT;
}

void TwoLayerNN::WAug_update( const Eigen::VectorXd& q, const Eigen::VectorXd& r, const Eigen::MatrixXd & sigmaPrime, double delT )
{
    W_trans_next.transpose() = W_trans.transpose()
                                 + ((   F*hiddenLayer_out*r.transpose()
                                      - F*sigmaPrime*V_trans*x*r.transpose()
                                      - kappa*F*r.norm()*W_trans.transpose()
                                      ) * delT );
}



}

