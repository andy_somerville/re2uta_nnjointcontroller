/*
 * NnController.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/NnController.hpp>
#include <re2uta/TwoLayerNN.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>
#include <boost/shared_ptr.hpp>


namespace re2uta
{


NnController::
NnController( int numInputs,
              double kappa,
              double kp,
              double kd,
              int    hiddenLayerSize,
              double firstLayerLearningRate,
              double lastLayerLearningRate )  //fFactor );
{
    int    inputDim        = numInputs * 2;
    int    outputDim       = numInputs;

    m_neuralNet.reset( new TwoLayerNN( inputDim, outputDim, hiddenLayerSize, kappa, kp, kd, firstLayerLearningRate, lastLayerLearningRate) );
}

Eigen::VectorXd
NnController::
calcControlAndLearn( const Eigen::VectorXd & currentState,
                     const Eigen::VectorXd & currentStateDeriv1,
                     const Eigen::VectorXd & desiredState,
                     const Eigen::VectorXd & desiredStateDeriv1,
                     double dtSecs )
{
    Eigen::VectorXd compensationOutput;
    Eigen::VectorXd error;
    Eigen::VectorXd robustifySignal;
    Eigen::VectorXd controlSignal;

    compensationOutput = m_neuralNet->NNAug_out( desiredState, currentState, desiredStateDeriv1, currentStateDeriv1, dtSecs );
    error              = m_neuralNet->getFilteredError();
    robustifySignal    = m_neuralNet->getRobustifyingSignal();
    controlSignal      = compensationOutput + m_neuralNet->kd*error - robustifySignal;

//    static int count = 0;
//
//    if( count > 1000 )
//    {
//        std::stringstream debugout;
//        debugout << "\nkd:       " << m_neuralNet->kd
//                 << "\nerror:    " << error.transpose()
//                 << "\nrobustify:" << robustifySignal.transpose()
//                 << "\ncomp:     " << compensationOutput.transpose()
//                 << "\nkd*error: "  <<  (m_neuralNet->kd*error).transpose();
//        std::cout << debugout.str();
//        count = 0;
//    }
//    count++;

    return controlSignal;
}

}
