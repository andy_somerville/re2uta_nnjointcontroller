/*
 * NnControllerTest.cpp
 *
 *  Created on: Apr 18, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/TwoLayerNN.hpp>

#include <std_msgs/Float64MultiArray.h>

#include <ros/ros.h>

int main( int argCount, char** argVec  )
{
    ros::init( argCount, argVec, "TwoLayerNN_test" );

    ros::NodeHandle node;
    ros::Publisher  floatPub;

    floatPub = node.advertise<std_msgs::Float64MultiArray>( "/debug", 1 );


    ROS_INFO_STREAM( "Test: " << ros::this_node::getName() );

    int    numValues              = 2;
    double kappa                  = 0.2;
    double kp                     = 0.5;
    double kd                     = 2*std::sqrt(kp);
    int    inputDim               = numValues * 2;
    int    outputDim              = numValues;
    int    hiddenLayerSize        = numValues * 5;
    double firstLayerLearningRate = 50;
    double lastLayerLearningRate  = 50;

    re2uta::TwoLayerNN twoLayerNN( inputDim, outputDim, hiddenLayerSize, kappa, kp, kd, firstLayerLearningRate, lastLayerLearningRate );

    Eigen::VectorXd currentState       = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd currentStateDeriv1 = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd prevState          = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd desiredState       = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd desiredStateDeriv1 = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd controlSignal      = Eigen::VectorXd::Zero( numValues );

    int i       = 0;
    int numIter = 50 ;

    std::cout.setf(std::ios::fixed );
    std::cout.precision(3);
    std::cout.width(10);
    std::cout.right;

    double     dt = (2*M_PI)/numIter;

//    for( i = 0; i < numIter * 5; ++ i )
    for( i = 0; ros::ok(); ++ i )
    {
        double angle = (2*M_PI)/numIter * i;
        for( int s = 0; s < numValues; ++s )
        {
            desiredState( s )       = std::sin( angle );
            desiredStateDeriv1( s ) = std::cos( angle ) * dt;
        }

        std::cout << std::endl ;
        std::cout << "Current State     before step:  " << i << "  : " <<  currentState.transpose()       << "\n";
        std::cout << "Current State dt  before step:  " << i << "  : " <<  currentStateDeriv1.transpose() << "\n";
        std::cout << "Desired State     before step:  " << i << "  : " <<  desiredState.transpose()       << "\n";
        std::cout << "Desired State dt  before step:  " << i << "  : " <<  desiredStateDeriv1.transpose() << "\n";

        Eigen::VectorXd nonlinearOut = twoLayerNN.NNAug_out( desiredState, currentState, desiredStateDeriv1, currentStateDeriv1, dt );
        Eigen::VectorXd linearError  = twoLayerNN.getFilteredError();
        controlSignal  = nonlinearOut + linearError;

        for( int s = 0; s < numValues; ++s )
        {
            currentState( s )       = std::sin( controlSignal(s) );
//            currentStateDeriv1( s ) = std::cos( controlSignal(s) ) * (controlSignal(s) - prevControlSignal(s))/dt;
            currentStateDeriv1( s ) = currentState(s) - prevState(s);
        }
        prevState = currentState;

        std::cout << "Control Signal    after step:  " << i << "  : " <<  controlSignal.transpose()      << "\n";

        std::cout << "Current State      after step:  " << i << "  : " <<  currentState.transpose()       << "\n";
        std::cout << "Current State dt   after step:  " << i << "  : " <<  currentStateDeriv1.transpose() << "\n";
        std::cout << "State error        after step:  " << i << "  : " <<  (desiredState       - currentState      ).transpose() << "\n";
        std::cout << "State dt error     after step:  " << i << "  : " <<  (desiredStateDeriv1 - currentStateDeriv1).transpose() << "\n";


        std_msgs::Float64MultiArray debugMsg;
        debugMsg.data.push_back( desiredState(0)       );
        debugMsg.data.push_back( desiredStateDeriv1(0) );
        debugMsg.data.push_back( currentState(0)       );
        debugMsg.data.push_back( currentStateDeriv1(0) );
        debugMsg.data.push_back( controlSignal(0)      );
        debugMsg.data.push_back( (desiredState       - currentState      )(0) );
        debugMsg.data.push_back( (desiredStateDeriv1 - currentStateDeriv1)(0) );

        floatPub.publish( debugMsg );
        ros::Duration(0.1).sleep();
        ros::spinOnce();
    }

    std::cout << std::endl ;
    std::cout << "Current State     after final step:  " << i << "  : " <<  currentState.transpose()       << "\n";
    std::cout << "Current State dt  after final step:  " << i << "  : " <<  currentStateDeriv1.transpose() << "\n";
    std::cout << "Desired State     after final step:  " << i << "  : " <<  desiredState.transpose()       << "\n";
    std::cout << "Desired State dt  after final step:  " << i << "  : " <<  desiredStateDeriv1.transpose() << "\n";
    std::cout << "State error       after step:        " << i << "  : " <<  (desiredState       - currentState      ).transpose() << "\n";
    std::cout << "State dt error    after step:        " << i << "  : " <<  (desiredStateDeriv1 - currentStateDeriv1).transpose() << "\n";
    std::cout << "Control Signal    after final step:  " << i << "  : " <<  controlSignal.transpose()      << "\n";
    std::cout << std::endl ;

    ros::spin();

    return 0;
}
