/*
 * NnControllerTest.cpp
 *
 *  Created on: Apr 18, 2013
 *      Author: andrew.somerville
 */

#include <sensor_msgs/JointState.h>
#include <ros/ros.h>

int main( int argCount, char** argVec  )
{
    ros::init( argCount, argVec, "NnController_test" );

    return 0;
}
