/*
 * NnControllerTest.cpp
 *
 *  Created on: Apr 18, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/NnController.hpp>

#include <ros/ros.h>

int main( int argCount, char** argVec  )
{
    ros::init( argCount, argVec, "NnController_test" );

    ROS_INFO_STREAM( "Test: " << ros::this_node::getName() );

    int    numValues = 2;
    double kappa = 0.07;
    double kp    = 3;
    double kd    = 3*std::sqrt(3);
    double firstLayerLearningRate = 30;     //FIXME grab from param
    double lastLayerLearningRate  = 20;     //FIXME grab from param
    re2uta::NnController nnController( numValues, kappa, kp, kd, 30, firstLayerLearningRate, lastLayerLearningRate );

    Eigen::VectorXd currentState       = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd currentStateDeriv1 = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd desiredState       = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd desiredStateDeriv1 = Eigen::VectorXd::Zero( numValues );
    Eigen::VectorXd controlSignal      = Eigen::VectorXd::Zero( numValues );
    double          dt                  = 1;

    int i       = 0;
    int numIter = 10 ;

    std::cout.setf(std::ios::fixed );
    std::cout.precision(3);
    std::cout.width(10);
    std::cout.right;

    for( i = 0; i < numIter; ++ i )
    {
        double angle = M_PI/numIter * i;

        for( int s = 0; s < numValues; ++s )
        {
            desiredState( s )       = std::sin( angle );
            desiredStateDeriv1( s ) = std::cos( angle );
        }


        std::cout << std::endl ;
        std::cout << "Current State     before step:  " << i << "  : " <<  currentState.transpose()       << "\n";
        std::cout << "Current State dt  before step:  " << i << "  : " <<  currentStateDeriv1.transpose() << "\n";
        std::cout << "Desired State     before step:  " << i << "  : " <<  desiredState.transpose()       << "\n";
        std::cout << "Desired State dt  before step:  " << i << "  : " <<  desiredStateDeriv1.transpose() << "\n";

        controlSignal = nnController.calcControlAndLearn( currentState, currentStateDeriv1, desiredState, desiredStateDeriv1, dt );

        for( int s = 0; s < numValues; ++s )
        {
            currentState( s )       = std::sin( controlSignal(s) );
            currentStateDeriv1( s ) = std::cos( controlSignal(s) );
        }
        std::cout << "Control Signal    after step:  " << i << "  : " <<  controlSignal.transpose()      << "\n";

        std::cout << "Current State      after step:  " << i << "  : " <<  currentState.transpose()       << "\n";
        std::cout << "Current State dt   after step:  " << i << "  : " <<  currentStateDeriv1.transpose() << "\n";
        std::cout << "State error        after step:  " << i << "  : " <<  (desiredState       - currentState      ).transpose() << "\n";
        std::cout << "State dt error     after step:  " << i << "  : " <<  (desiredStateDeriv1 - currentStateDeriv1).transpose() << "\n";

    }

    std::cout << std::endl ;
    std::cout << "Current State     after final step:  " << i << "  : " <<  currentState.transpose()       << "\n";
    std::cout << "Current State dt  after final step:  " << i << "  : " <<  currentStateDeriv1.transpose() << "\n";
    std::cout << "Desired State     after final step:  " << i << "  : " <<  desiredState.transpose()       << "\n";
    std::cout << "Desired State dt  after final step:  " << i << "  : " <<  desiredStateDeriv1.transpose() << "\n";
    std::cout << "State error       after step:        " << i << "  : " <<  (desiredState       - currentState      ).transpose() << "\n";
    std::cout << "State dt error    after step:        " << i << "  : " <<  (desiredStateDeriv1 - currentStateDeriv1).transpose() << "\n";
    std::cout << "Control Signal    after final step:  " << i << "  : " <<  controlSignal.transpose()      << "\n";
    std::cout << std::endl ;

    return 0;
}
