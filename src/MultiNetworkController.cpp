/*
 * MultiNetworkController.cpp
 *
 *  Created on: Jun 4, 2013
 *      Author: andrew.somerville
 */

#include <re2uta/MultiNetworkController.hpp>

#include <atlas_msgs/SetJointDamping.h>

#include <re2uta/TwoLayerNN.hpp>
#include <re2uta/NnController.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <atlas_msgs/AtlasState.h>
#include <atlas_msgs/AtlasCommand.h>
#include <re2uta/AtlasLookup.hpp>
#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/eigen/eigen_util.h>

#include <ros/ros.h>
#include <boost/tuple/tuple.hpp>
#include <string>
#include <math.h>


namespace
{
    template <typename Type>
    Type exponentialWeightedAverage( Type oldVal, Type newVal, double newWeight )
    {
        return oldVal*(1-newWeight) + newWeight*(newVal);
    }

    template <typename Type>
    Type lowPassFilter( Type oldVal, Type newVal, double newWeight )
    {
        return exponentialWeightedAverage( oldVal, newVal, newWeight );
    }

    Eigen::VectorXd subsetInclusive( int baseIndex, int tipIndex, const Eigen::VectorXd & inputVector )
    {
        Eigen::VectorXd outputVector( std::abs( tipIndex - baseIndex  + 1 ) );

        int count = 0;
        for( int i = baseIndex; i <= tipIndex; ++i )
        {
            outputVector( count ) = inputVector( i );
            ++count;
        }

        return outputVector;
    }


    Eigen::VectorXd setSubsetInclusive( int baseIndex, int tipIndex, const Eigen::VectorXd & inputVector, Eigen::VectorXd & outputVector )
    {
        int count = 0;
        for( int i = baseIndex; i <= tipIndex; ++i )
        {
            outputVector( i ) = inputVector( count );
            ++count;
        }

        return outputVector;
    }
}


namespace re2uta
{


MultiNetworkController::
MultiNetworkController( int numJoints, double posFilterGain, double velFilterGain )
    : m_numJoints( numJoints )
{
    m_filterPosGain = posFilterGain;
    m_filterVelGain = velFilterGain;
}


void
MultiNetworkController::
addController(
    boost::tuple<int,int> const & range,
    double kappa,
    double kp,
    double kd,
    int hiddenLayerSize,
    double firstLayerLearningRate,
    double lastLayerLearningRate )
{
    int numJoints = std::abs( range.get<1>() - range.get<0>() ) + 1;
    m_controllerRanges.push_back( range );
    m_controllers.push_back(  NnController::Ptr( new NnController( numJoints,
                                                                   kappa,
                                                                   kp,
                                                                   kd,
                                                                   hiddenLayerSize,
                                                                   firstLayerLearningRate,
                                                                   lastLayerLearningRate ) ) );
}


void
MultiNetworkController::
setStateMsg( const atlas_msgs::AtlasStateConstPtr & stateMsg )
{
    boost::atomic_store( &m_lastAtlasState, stateMsg );
}


void
MultiNetworkController::
setCommandMsg( const atlas_msgs::AtlasCommandConstPtr & commandMsg )
{
    boost::atomic_store( &m_lastAtlasCommand, commandMsg );
}


void
MultiNetworkController::
updateParams( const atlas_msgs::AtlasStateConstPtr & stateMsg,
              const atlas_msgs::AtlasCommandConstPtr & commandMsg,
              double dt )
{
    Eigen::VectorXd newPositions  = re2uta::parseJointPositions(  stateMsg );
    Eigen::VectorXd newVelocities = re2uta::parseJointVelocities( stateMsg );

    if( m_positions.size() == 0 )
        m_positions = newPositions;
    if( m_velocities.size() == 0 )
        m_velocities = newVelocities;

    m_positions  = lowPassFilter( m_positions,  newPositions,  m_filterPosGain*dt );
    m_velocities = lowPassFilter( m_velocities, newVelocities, m_filterVelGain*dt );

    if( commandMsg )
    {
        m_desiredPositions  = re2uta::parseJointPositions(  commandMsg );
        m_desiredVelocities = re2uta::parseJointVelocities( commandMsg );
    }
    else
    if( m_desiredPositions.size() == 0 )
    {
        m_desiredPositions  = m_positions;
        m_desiredVelocities = m_velocities;
    }
}

Eigen::VectorXd
MultiNetworkController::
calcTorques( double dt )
{
    atlas_msgs::AtlasStateConstPtr   atlasState;
    atlas_msgs::AtlasCommandConstPtr atlasCommand;
    atlasState   = boost::atomic_load( &m_lastAtlasState   );
    atlasCommand = boost::atomic_load( &m_lastAtlasCommand );


    if( !atlasState ) // || !atlasCommand )
        return Eigen::VectorXd();

    updateParams( atlasState, atlasCommand, dt );



    Eigen::VectorXd systemTorques = Eigen::VectorXd::Zero( m_numJoints );

    int index = 0;
    BOOST_FOREACH( re2uta::NnController::Ptr controller, m_controllers )
    {
        boost::tuple<int,int> & range = m_controllerRanges[index];

        Eigen::VectorXd positions          = subsetInclusive( range.get<0>(), range.get<1>(), m_positions         );
        Eigen::VectorXd velocities         = subsetInclusive( range.get<0>(), range.get<1>(), m_velocities        );
        Eigen::VectorXd desiredPositions   = subsetInclusive( range.get<0>(), range.get<1>(), m_desiredPositions  );
        Eigen::VectorXd desiredVelocities  = subsetInclusive( range.get<0>(), range.get<1>(), m_desiredVelocities );

        Eigen::VectorXd controllerTorques;
        controllerTorques = controller->calcControlAndLearn( positions,        velocities,
                                                             desiredPositions, desiredVelocities,
                                                             dt );

        setSubsetInclusive( range.get<0>(), range.get<1>(), controllerTorques , systemTorques );

        ++index;
    }


    return systemTorques;
}


}



/*
void test()
{
    Eigen::VectorXd torques = Eigen::VectorXd::Zero( 28 );
    Eigen::VectorXd result  = Eigen::VectorXd::Zero( 28 );
    Eigen::VectorXd leftArmTorques(  6 );
    Eigen::VectorXd rightArmTorques( 6 );
    Eigen::VectorXd leftLegTorques(  6 );
    Eigen::VectorXd rightLegTorques( 6 );
    Eigen::VectorXd backTorques(     3 );

    for( int i = 0; i < 28 ; ++i )
        torques( i ) = i;

    typedef atlas_msgs::AtlasState Atlas;
    leftArmTorques  = subsetInclusive( Atlas::l_arm_shy, Atlas::l_arm_wrx, torques );
    rightArmTorques = subsetInclusive( Atlas::r_arm_shy, Atlas::r_arm_wrx, torques );
    leftLegTorques  = subsetInclusive( Atlas::l_leg_hpz, Atlas::l_leg_akx, torques );
    rightLegTorques = subsetInclusive( Atlas::r_leg_hpz, Atlas::r_leg_akx, torques );
    backTorques     = subsetInclusive( Atlas::back_bkz,  Atlas::back_bkx,  torques );

    std::cout << "larm torques " << leftArmTorques.transpose()  << std::endl;
    std::cout << "rarm torques " << rightArmTorques.transpose() << std::endl;
    std::cout << "lleg torques " << leftLegTorques.transpose()  << std::endl;
    std::cout << "rleg torques " << rightLegTorques.transpose() << std::endl;
    std::cout << "back torques " << backTorques.transpose()     << std::endl;


    setSubsetInclusive( Atlas::l_arm_shy, Atlas::l_arm_wrx, leftArmTorques , result );
    setSubsetInclusive( Atlas::r_arm_shy, Atlas::r_arm_wrx, rightArmTorques, result );
    setSubsetInclusive( Atlas::l_leg_hpz, Atlas::l_leg_akx, leftLegTorques , result );
    setSubsetInclusive( Atlas::r_leg_hpz, Atlas::r_leg_akx, rightLegTorques, result );
    setSubsetInclusive( Atlas::back_bkz,  Atlas::back_bkx,  backTorques    , result );

    std::cout << "result torques\n" << result << std::endl;
} */

