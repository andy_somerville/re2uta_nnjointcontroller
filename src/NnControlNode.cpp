/*
 * NnControlNode.cpp
 *
 *  Created on: Apr 16, 2013
 *      Author: andrew.somerville
 */


#include <re2uta/NnController.hpp>
#include <re2uta/AtlasLookup.hpp>
#include <re2uta/atlas_msg_utils.hpp>

#include <re2/kdltools/kdl_tree_util.hpp>
#include <re2/kdltools/kdl_tools.h>

#include <kdl/tree.hpp>

#include <osrf_msgs/JointCommands.h>
#include <sensor_msgs/JointState.h>

#include <ros/ros.h>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <boost/shared_ptr.hpp>
#include <boost/foreach.hpp>


using namespace re2uta;


class NnControlNode
{
    public:
        NnControlNode( const Eigen::VectorXd & defaultJointAngles,
                       double kappa,
                       double kp,
                       double kd,
                       double firstLayerLearningRate,
                       double lastLayerLearningRate )
        {
            m_lastTime             = ros::Time::now();

            std::string robotUrdfString;
            m_node.getParam( "robot_description", robotUrdfString );

            KDL::Tree tree;
            kdlRootInertiaWorkaround( robotUrdfString, tree );

            urdf::Model model;
            model.initString( robotUrdfString );

            m_atlasLookup.reset( new AtlasLookup(model) );

            assert( defaultJointAngles.size() == m_atlasLookup->getJointNameVector().size() );
            m_numJoints              = defaultJointAngles.size();

            m_desiredJointAngles     = defaultJointAngles;
            m_desiredJointVelocities = Eigen::VectorXd::Zero( m_numJoints );
            m_nnController.reset( new NnController( m_numJoints, kappa, kp, kd, 20*m_numJoints, firstLayerLearningRate, lastLayerLearningRate ) );

            m_defaultJointsCommand = *buildDefaultZeroedJointsCommand( m_atlasLookup->getJointNameVector() );

            m_jointCmdPub    = m_node.advertise<osrf_msgs::JointCommands>( "/atlas/joint_commands", 1, true );
            m_jointCmdSub    = m_node.subscribe( "walk/joint_commands", 1, &NnControlNode::handleJointCmd,    this );
            m_jointStatesSub = m_node.subscribe( "/atlas/joint_states", 1, &NnControlNode::handleJointStates, this );
        }


    private:
        void handleJointCmd( const osrf_msgs::JointCommands::ConstPtr & jointCmd )
        {
            int msgJointIndex = 0;
            BOOST_FOREACH( const std::string & name, jointCmd->name )
            {
                int cmdJointIndex = m_atlasLookup->jointNameToJointIndex( name.substr(5,std::string::npos) ); // probably unnecessary but safer

                if( cmdJointIndex >= 0 && cmdJointIndex < m_numJoints )
                {
                    m_desiredJointAngles(     cmdJointIndex ) = jointCmd->position[ msgJointIndex ];
                    m_desiredJointVelocities( cmdJointIndex ) = jointCmd->velocity[ msgJointIndex ];
                }

                ++msgJointIndex;
            }
        }


        void handleJointStates( const sensor_msgs::JointStateConstPtr & jointStateMsg )
        {
            ros::Time     now;
            ros::Duration dt;

            now = ros::Time::now();
            dt  = now - m_lastTime;

            double minTimeStep = 0;
            if( dt.toSec() <= minTimeStep ) // avoid oversampling if we set minTimeStep to non-zero
                return;

            m_lastTime = now;

            Eigen::VectorXd jointAngles(     m_numJoints );
            Eigen::VectorXd jointVelocities( m_numJoints );


            int msgJointIndex = 0;
            BOOST_FOREACH( const std::string & name, jointStateMsg->name )
            {
                int cmdJointIndex = m_atlasLookup->jointNameToJointIndex( name ); //FIXME make sure we're taking care of atlas::

                if( cmdJointIndex >= 0 && cmdJointIndex < m_numJoints )
                {
                    jointAngles( cmdJointIndex )     = jointStateMsg->position[ msgJointIndex ];
                    jointVelocities( cmdJointIndex ) = jointStateMsg->velocity[ msgJointIndex ];
                }
                else
                    ROS_WARN( "bad joint %i, %s", cmdJointIndex, name.c_str() );

                ++msgJointIndex;
            }

            Eigen::VectorXd jointTorqueCmd;
            jointTorqueCmd = m_nnController->calcControlAndLearn( jointAngles,          jointVelocities,
                                                                  m_desiredJointAngles, m_desiredJointVelocities,
                                                                  dt.toSec() );

            commandJoints( jointTorqueCmd );
        }


        void commandJoints( const Eigen::VectorXd & jointTorqueCmd )
        {
            osrf_msgs::JointCommands jointsCommand;
            jointsCommand = m_defaultJointsCommand;

            jointsCommand.header.stamp = ros::Time::now();

            for( int cmdJointIndex = 0; cmdJointIndex < jointTorqueCmd.rows(); ++ cmdJointIndex )
            {
                jointsCommand.effort[ cmdJointIndex ] = jointTorqueCmd( cmdJointIndex );
                ++ cmdJointIndex;
            }

            m_jointCmdPub.publish( jointsCommand );
        }

    private:
        ros::NodeHandle           m_node;
        int                       m_numJoints;
        NnController::Ptr         m_nnController;
        ros::Time                 m_lastTime;
        Eigen::VectorXd           m_desiredJointAngles;
        Eigen::VectorXd           m_desiredJointVelocities;
        osrf_msgs::JointCommands  m_defaultJointsCommand;
        ros::Publisher            m_jointCmdPub;
        ros::Subscriber           m_jointCmdSub;
        ros::Subscriber           m_jointStatesSub;
        AtlasLookup::Ptr          m_atlasLookup;


};


int main(int argc, char** argv)
{
    ros::init(argc, argv, "nn_control_node");

    ros::NodeHandle node;

    std::string urdfString;
    node.getParam( "/robot_description", urdfString );

    urdf::Model model;
    model.initString( urdfString );

    AtlasLookup::Ptr atlasLookup;
    atlasLookup.reset( new AtlasLookup( model ) );

    Eigen::VectorXd defaultJointAngles; //FIXME grab from topic or param

    double legBend = 0.4;
    defaultJointAngles = atlasLookup->calcDefaultTreeJointAngles( legBend );

//    Eigen::VectorXd jointMins = atlasLookup->getJointMins();
//    Eigen::VectorXd jointMaxs = atlasLookup->getJointMaxs();
//
//    Eigen::VectorXd jointMidPoints =  ((jointMaxs + jointMins)/2).unaryExpr( std::ptr_fun( nonNaN ) );
//    defaultJointAngles = jointMidPoints;
//
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "head"    ) ) = 0.8;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "l_lleg"  ) ) =  legBend * 2;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "r_lleg"  ) ) =  legBend * 2;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "l_talus" ) ) = -legBend;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "r_talus" ) ) = -legBend;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "l_uleg"  ) ) = -legBend - 0.02;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "r_uleg"  ) ) = -legBend - 0.02;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "l_uglut" ) ) =  0;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "r_uglut" ) ) =  0;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "l_lglut" ) ) =  0;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "r_lglut" ) ) =  0;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "r_scap"  ) ) =  1.3;
//    defaultJointAngles( atlasLookup->segmentToJointIndex( "l_scap"  ) ) = -1.3;


    double kappa                  =  0.03; //FIXME grab from topic or param
    double kp                     =  4;     //FIXME grab from topic or param
    double kd                     =  0.8;   //FIXME grab from topic or param
    double firstLayerLearningRate = 30;     //FIXME grab from param
    double lastLayerLearningRate  = 20;     //FIXME grab from param

    NnControlNode nnControlNode( defaultJointAngles,
                                 kappa,
                                 kp,
                                 kd,
                                 firstLayerLearningRate,
                                 lastLayerLearningRate );

    ros::spin();
}




