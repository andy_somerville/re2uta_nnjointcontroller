/*
 * NnController.hpp
 *
 *  Created on: Apr 18, 2013
 *      Author: Ghassan Atmeh, Isura Ranatunga, andrew.somerville
 */

#pragma once

#include <re2uta/TwoLayerNN.hpp>
#include <Eigen/Core>
#include <boost/shared_ptr.hpp>

namespace re2uta
{

class NnController
{
    public:
        typedef boost::shared_ptr<NnController> Ptr;

    public:
        NnController( int    numInputs,
                      double kappa,
                      double kp,
                      double kd,
                      int    hiddenLayerSize,
                      double firstLayerLearningRate,
                      double lastLayerLearningRate );

        Eigen::VectorXd
        calcControlAndLearn( const Eigen::VectorXd & currentState,
                             const Eigen::VectorXd & currentStateDeriv1,
                             const Eigen::VectorXd & desiredState,
                             const Eigen::VectorXd & desiredStateDeriv1,
                             double dtSecs );


    private:
        TwoLayerNN::Ptr m_neuralNet;

};

}


